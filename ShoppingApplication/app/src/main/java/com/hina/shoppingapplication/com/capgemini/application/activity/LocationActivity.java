package com.hina.shoppingapplication.com.capgemini.application.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.hina.shoppingapplication.R;
import com.hina.shoppingapplication.com.capgemini.application.activity.MainActivity;


/*
*
* Created By:Sandeep
*
* The Location will take the value and set in the location in the navigation bar
*
* */
public class LocationActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        Button button = (Button) findViewById(R.id.btn_setlocation);
        final EditText edt_location = (EditText) findViewById(R.id.edt_enterlocation);

        //Controls Onclick function of button btn_setLocation
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                //Defining new Intent to pass location
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);

                /*Gets the string of entered location in EditText and returns it
                 to MainActivity to Navigation Bar LocationActivity Menu using intent.putExtra*/
                intent.putExtra("location", edt_location.getText().toString());
                startActivity(intent);
            }
        });

    }

}
