package com.hina.shoppingapplication.com.capgemini.application.model;

/*
 * Created By:Vineetha and Divya
 *
 * The ProductModel pojo class setting and getting the values
 *
 * */

public class ProductModel {

    private String name;
    private int image_drawable;
    private String price;
    private String offer;
    private String strike_price;
    private String quantity;
    private String clubPrice;



    public String getClubPrice() {
        return clubPrice;
    }

    public void setClubPrice(String clubPrice) {
        this.clubPrice = clubPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public String getStrike_price() {
        return strike_price;
    }

    public void setStrike_price(String strike_price) {
        this.strike_price = strike_price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public int getImage_drawable() {
        return image_drawable;
    }

    public void setImage_drawable(int image_drawable) {
        this.image_drawable = image_drawable;
    }
}
