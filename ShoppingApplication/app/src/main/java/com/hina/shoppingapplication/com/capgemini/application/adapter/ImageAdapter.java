package com.hina.shoppingapplication.com.capgemini.application.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.hina.shoppingapplication.com.capgemini.application.model.ProductImages;
import com.hina.shoppingapplication.R;

import java.util.ArrayList;

/*
 * Created By:Hina
 *
 * This adapter will set data images list in the Carousal view
 *
 *
 * */

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<ProductImages> imageModelArrayList;


    public ImageAdapter(Context ctx, ArrayList<ProductImages> imageModelArrayList){

        inflater = LayoutInflater.from(ctx);
        this.imageModelArrayList = imageModelArrayList;
    }

    @Override
    public ImageAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.activity_product_description_recycler_view, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(ImageAdapter.MyViewHolder holder, int position) {

        holder.imageAdapterIv.setImageResource(imageModelArrayList.get(position).getImage_drawable());
    }

    @Override
    public int getItemCount() {
        return imageModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView imageAdapterIv;

        public MyViewHolder(View itemView) {
            super(itemView);
            imageAdapterIv = (ImageView) itemView.findViewById(R.id.iv);
        }

    }

}
