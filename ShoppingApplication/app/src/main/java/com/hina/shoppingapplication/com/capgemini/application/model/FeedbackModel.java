package com.hina.shoppingapplication.com.capgemini.application.model;

/*
* Created By:Nisha
*
* The FeedbackModel pojo class setting and getting the values
*
* */

public class FeedbackModel {

    private String fbCustomername;
    private int fbCustomerImage;
    private String message;


    public String getName() {

        return fbCustomername;
    }
    public void setName(String name) {

        this.fbCustomername = fbCustomername;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public int getFbCustomerImage() {

        return fbCustomerImage;
    }

    public void setFbCustomerImage(int fbCustomerImage) {
        this.fbCustomerImage = fbCustomerImage;
    }
}
