package com.hina.shoppingapplication.com.capgemini.application.activity;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hina.shoppingapplication.R;

/*
* Created By: Nisha
*
*  Login Page will take the authentication and navigate the page to the main screen by showing the notification at the top
*
* */

public class Login extends AppCompatActivity {

    Button btnLogin;
    EditText edtPhoneNumber;

    private NotificationManager notificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        btnLogin = (Button) findViewById(R.id.login_button);
        edtPhoneNumber = (EditText) findViewById(R.id.edt_phoneNo);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtPhoneNumber.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Please enter phone number", Toast.LENGTH_SHORT).show();
                } else if (edtPhoneNumber.getText().toString().equals("7903212060")||
                        edtPhoneNumber.getText().toString().equals("9405820460")||
                        edtPhoneNumber.getText().toString().equals("8498847179") ||
                        edtPhoneNumber.getText().toString().equals("9110743262")||
                        edtPhoneNumber.getText().toString().equals("9764361856")||
                        edtPhoneNumber.getText().toString().equals("7661095305")) {
                    createLocalNotification(getResources().getString(R.string.app_name), Login.this);
                    Intent intent = new Intent(Login.this, MainActivity.class);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), "Redirecting...", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Invalid number", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void createLocalNotification(String message, Context context) {

        final int NOTIFY_ID = 0; // ID of notification
        String id = "0034"; // default_channel_id
        String title = "CHANNEL"; // Default Channel
        Intent intent;
        PendingIntent pendingIntent;
        NotificationCompat.Builder builder;

        /*intent = new Intent(this, NotificationReceiverActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);*/

        if (notificationManager == null) {
            notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = notificationManager.getNotificationChannel(id);

            if (notificationChannel == null) {
                notificationChannel = new NotificationChannel(id, title, importance);
                notificationChannel.enableVibration(true);
                //notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                notificationManager.createNotificationChannel(notificationChannel);
            }

            builder = new NotificationCompat.Builder(context, id);

            builder.setContentTitle(message)                            // required
                    .setSmallIcon(android.R.drawable.ic_popup_reminder)   // required
                    .setContentText("Welcome to Grofers. Happy Shopping!!") // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    /*.setContentIntent(pendingIntent)*/
                    .setTicker(message)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        } else {
            builder = new NotificationCompat.Builder(context, id);

            builder.setContentTitle(message)                            // required
                    .setSmallIcon(android.R.drawable.ic_popup_reminder)   // required
                    .setContentText("Test Notification") // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    /*.setContentIntent(pendingIntent)*/
                    .setTicker(message)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                    .setPriority(Notification.PRIORITY_HIGH);
        }

        Notification notification = builder.build();
        notificationManager.notify(NOTIFY_ID, notification); //for pushing the notification
    }

}
