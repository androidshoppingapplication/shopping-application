package com.hina.shoppingapplication.com.capgemini.application.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hina.shoppingapplication.com.capgemini.application.activity.ProductDescription;
import com.hina.shoppingapplication.com.capgemini.application.model.ProductModel;
import com.hina.shoppingapplication.R;

import java.util.ArrayList;

/*
 * Created By: Srikanth and Divya
 *
 * This adapter will set all the data of image, discount, price, actual price and quantity  in the recycler view
 * of the Best Selling Items, Top Saver Items and Block Buster Items
 *
 * */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {

    private final Context ctx;
    private LayoutInflater inflater;
    private ArrayList<ProductModel> imageModelArrayList;


    public ProductAdapter(Context ctx, ArrayList<ProductModel> imageModelArrayList) {

        this.ctx=ctx;
        inflater = LayoutInflater.from(ctx);
        this.imageModelArrayList = imageModelArrayList;
    }

    @Override
    public ProductAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.recycler_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(ProductAdapter.MyViewHolder holder, int position) {

        holder.productIv.setImageResource(imageModelArrayList.get(position).getImage_drawable());
        holder.productTv.setText(imageModelArrayList.get(position).getName());
        holder.price.setText(imageModelArrayList.get(position).getPrice());
        holder.offer.setText(imageModelArrayList.get(position).getOffer());
        holder.strikeOffPrice.setText(imageModelArrayList.get(position).getStrike_price());
        holder.quantity.setText(imageModelArrayList.get(position).getQuantity());
   //     holder.clubPrice.setText(imageModelArrayList.get(position).getClubPrice());

        final int p=position;
       // on click of the card view it goes to the description page by taking the value of the particular card view name
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ctx, ProductDescription.class);
                String text= imageModelArrayList.get(p).getName();
                intent.putExtra("Name",text);
                ctx.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return imageModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView productTv;
        ImageView productIv;
        TextView price;
        TextView offer;
        TextView strikeOffPrice;
        TextView quantity;
        TextView clubPrice;
        CardView card;

        public MyViewHolder(View itemView) {
            super(itemView);

            productTv = (TextView) itemView.findViewById(R.id.tv);
            productIv = (ImageView) itemView.findViewById(R.id.iv);
            price = (TextView) itemView.findViewById(R.id.price);
            offer = itemView.findViewById(R.id.home_offer_id);
            strikeOffPrice = itemView.findViewById(R.id.strike_off_price);
            quantity = itemView.findViewById(R.id.quan);
            clubPrice = itemView.findViewById(R.id.txt_offPrice);
            card=(CardView) itemView.findViewById(R.id.card_view);
        }

    }
}
