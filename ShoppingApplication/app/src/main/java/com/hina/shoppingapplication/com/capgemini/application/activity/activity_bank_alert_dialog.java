package com.hina.shoppingapplication.com.capgemini.application.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hina.shoppingapplication.R;

public class activity_bank_alert_dialog extends AppCompatActivity {

    private TextView bank;
    private ImageView icon;
    private TextView data;
    private Button close;
    AlertDialog dialog;
    AlertDialog.Builder mBuild;
    View mView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String bankName=getIntent().getStringExtra("Bank");
        switch(bankName)
        {
            case "Axis":
                mBuild = new AlertDialog.Builder(this);
                mView = getLayoutInflater().inflate(R.layout.activity_bank_alert_dialog,null);
                bank = (TextView)mView.findViewById(R.id.bank_name);
                icon = (ImageView) mView.findViewById(R.id.bank_image);
                data = (TextView) mView.findViewById(R.id.bank_text);
                close = (Button) mView.findViewById(R.id.btn_bank_close);

                bank.setText("AXIS");
                icon.setImageResource(R.drawable.axis_bank);
                data.setText(" It has 25% of the offer by purchasing through this bank");
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent1 = new Intent(activity_bank_alert_dialog.this, MainActivity.class);
                        startActivity(intent1);
                    }
                });
                mBuild.setView(mView);
                dialog=mBuild.create();
                dialog.show();
                break;
            case "ICICI":
                mBuild = new AlertDialog.Builder(this);
                mView = getLayoutInflater().inflate(R.layout.activity_bank_alert_dialog,null);
                bank = (TextView)mView.findViewById(R.id.bank_name);
                icon = (ImageView) mView.findViewById(R.id.bank_image);
                data = (TextView) mView.findViewById(R.id.bank_text);
                close = (Button) mView.findViewById(R.id.btn_bank_close);
                bank.setText("ICICI");
                icon.setImageResource(R.drawable.icici_bank);
                data.setText(" It has 18% of the offer by purchasing through this bank");
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent1 = new Intent(activity_bank_alert_dialog.this, MainActivity.class);
                        startActivity(intent1);
                    }
                });
                mBuild.setView(mView);
                dialog=mBuild.create();
                dialog.show();
                break;
            case "HDFC":
                mBuild = new AlertDialog.Builder(this);
                mView = getLayoutInflater().inflate(R.layout.activity_bank_alert_dialog,null);
                bank = (TextView)mView.findViewById(R.id.bank_name);
                icon = (ImageView) mView.findViewById(R.id.bank_image);
                data = (TextView) mView.findViewById(R.id.bank_text);
                close = (Button) mView.findViewById(R.id.btn_bank_close);
                bank.setText("HDFC");
                icon.setImageResource(R.drawable.hdfc_bank);
                data.setText(" It has 40% of the offer by purchasing through this bank");
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent1 = new Intent(activity_bank_alert_dialog.this, MainActivity.class);
                        startActivity(intent1);
                    }
                });
                mBuild.setView(mView);
                dialog=mBuild.create();
                dialog.show();
                break;
            case "IDBI":
                mBuild = new AlertDialog.Builder(this);
                mView = getLayoutInflater().inflate(R.layout.activity_bank_alert_dialog,null);
                bank = (TextView)mView.findViewById(R.id.bank_name);
                icon = (ImageView) mView.findViewById(R.id.bank_image);
                data = (TextView) mView.findViewById(R.id.bank_text);
                close = (Button) mView.findViewById(R.id.btn_bank_close);
                bank.setText("IDBI");
                icon.setImageResource(R.drawable.idbi_bank);
                data.setText(" It has 10% of the offer by purchasing through this bank");
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent1 = new Intent(activity_bank_alert_dialog.this, MainActivity.class);
                        startActivity(intent1);
                    }
                });
                mBuild.setView(mView);
                dialog=mBuild.create();
                dialog.show();
                break;
            case "Kotak":
                mBuild = new AlertDialog.Builder(this);
                mView = getLayoutInflater().inflate(R.layout.activity_bank_alert_dialog,null);
                bank = (TextView)mView.findViewById(R.id.bank_name);
                icon = (ImageView) mView.findViewById(R.id.bank_image);
                data = (TextView) mView.findViewById(R.id.bank_text);
                close = (Button) mView.findViewById(R.id.btn_bank_close);
                bank.setText("Kotak Bank");
                icon.setImageResource(R.drawable.kotak_bank);
                data.setText(" It has 27% of the offer by purchasing through this bank");
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent1 = new Intent(activity_bank_alert_dialog.this, MainActivity.class);
                        startActivity(intent1);
                    }
                });
                mBuild.setView(mView);
                dialog=mBuild.create();
                dialog.show();
                break;
            case "RBL":
                mBuild = new AlertDialog.Builder(this);
                mView = getLayoutInflater().inflate(R.layout.activity_bank_alert_dialog,null);
                bank = (TextView)mView.findViewById(R.id.bank_name);
                icon = (ImageView) mView.findViewById(R.id.bank_image);
                data = (TextView) mView.findViewById(R.id.bank_text);
                close = (Button) mView.findViewById(R.id.btn_bank_close);
                bank.setText("RBL");
                icon.setImageResource(R.drawable.rbl_bank);
                data.setText(" It has 14% of the offer by purchasing through this bank");
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent1 = new Intent(activity_bank_alert_dialog.this, MainActivity.class);
                        startActivity(intent1);
                    }
                });
                mBuild.setView(mView);
                dialog=mBuild.create();
                dialog.show();
                break;
        }



    }
}
