package com.hina.shoppingapplication.com.capgemini.application.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hina.shoppingapplication.R;
import com.hina.shoppingapplication.com.capgemini.application.adapter.ImageAdapter;
import com.hina.shoppingapplication.com.capgemini.application.adapter.RowCustomAdapter;
import com.hina.shoppingapplication.com.capgemini.application.db.DataBaseHelper;
import com.hina.shoppingapplication.com.capgemini.application.model.ProductImages;
import com.hina.shoppingapplication.com.capgemini.application.model.RowData;

import java.util.ArrayList;


/*
* Created By:Vineetha and Divya
*
* In this all the data of the particular product will be shown
*
* */
public class ProductDescription extends AppCompatActivity {


    // initialized all the views
    DataBaseHelper dataBaseHelper;

    private TextView productName;
    private ImageView productImage;
    private ImageView productDescriptionImage1;
    private ImageView productDescriptionImage2;
    private TextView discountOffer;
    private TextView price;
    private TextView quantity;
    private TextView offPrice;
    private TextView highlight;
    private TextView info;
    private ListView list;
    ArrayList<RowData> highlightList;
    ArrayList<RowData> infoList;
    private ImageAdapter adapter;
    private RecyclerView recyclerView;
    private ArrayList<ProductImages> imageModelArrayList;
    int[] myRecycleList;
    int result;

    private Button addBtn;
    private Button minusBtn;
    //private Button buttonback;

    int count = 0;
    private TextView txt_display;
    private TextView cart1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_screen);

        //buttonback=findViewById(R.id.btn_back);
        addBtn = (Button) findViewById(R.id.add_btn1);
        minusBtn = (Button) findViewById(R.id.minus_btn1);
        txt_display = (TextView) findViewById(R.id.addItem);
        cart1 = (TextView)findViewById(R.id.cartId1);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        Intent intent=getIntent();
        String name= intent.getStringExtra("Name");

        productName=(TextView)findViewById(R.id.product_name_id);
        productName.setText(name);

        discountOffer = (TextView) findViewById(R.id.discount_id);
        price = (TextView) findViewById(R.id.price_id);
        quantity = (TextView) findViewById(R.id.quantity_id);
        highlight = (TextView) findViewById(R.id.highlights_id);
        info = (TextView) findViewById(R.id.info_id);
        offPrice = (TextView) findViewById(R.id.txt_offPrice);
        //Setting the text of the discountOffer,price,quantity by taking the data from the database
        // giving the description images in the array list taking the data from the database
        dataBaseHelper = new DataBaseHelper(this);
        Cursor res= dataBaseHelper.getDataOfProducts(name);
        while(res.moveToNext()){

         //   result=Integer.parseInt(res.getString(5))*(95/100);
            discountOffer.setText(res.getString(7));
            price.setText(res.getString(5));
            quantity.setText(res.getString(6));
            offPrice.setText(res.getString(15));
            myRecycleList=new int[]{res.getInt(1),res.getInt(2),res.getInt(3)};

        }

        init();


        // Seeting the Default Adapter and showing the highlight list by default
        RowCustomAdapter rowCustomAdapter = new RowCustomAdapter(this,0,highlightList);
        list.setAdapter(rowCustomAdapter);


        // On Click of the highlight text it shows the hightlight list
        highlight.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        RowCustomAdapter rowCustomAdapter = new RowCustomAdapter(ProductDescription.this,0,highlightList);
                        list.setAdapter(rowCustomAdapter);
                    }
                }
        );

        // On Click of the info text it shows the info list
        info.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        RowCustomAdapter rowCustomAdapter = new RowCustomAdapter(ProductDescription.this,0,infoList);
                        list.setAdapter(rowCustomAdapter);
                    }
                }
        );

        //Recycler view implementation

        recyclerView = (RecyclerView) findViewById(R.id.recycler);


        imageModelArrayList = imageList();
        adapter = new ImageAdapter(this, imageModelArrayList);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));


        addBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        count++;
                        txt_display.setText(String.valueOf(count));

                    }
                }
        );


        if(count>0){
            SharedPreferences sharedPreferences=getSharedPreferences("Cart List", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor=sharedPreferences.edit();
            editor.putInt("CartCount",count);
            editor.commit();
        }else{
            SharedPreferences sharedPreferences=getSharedPreferences("Cart List",Context.MODE_PRIVATE);
            SharedPreferences.Editor editor=sharedPreferences.edit();
            editor.putInt("CartCount",0);
            editor.commit();
        }

        minusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count--;
                if(count<0){
                    count=0;
                    Toast.makeText(ProductDescription.this,"No items in the cart to be deducted further",Toast.LENGTH_LONG).show();
                }
                txt_display.setText(String.valueOf(count));


            }
        });

      /*  buttonback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentmain = new Intent(ProductDescription.this, MainActivity.class);
                startActivity(intentmain);
            }
        });*/
    }

    //Recycler view product images array list
    private ArrayList<ProductImages> imageList(){

        ArrayList<ProductImages> list = new ArrayList<>();

        for(int i = 0; i < 3; i++){
            ProductImages image=new ProductImages();
            image.setImage_drawable(myRecycleList[i]);
            list.add(image);
        }

        return list;
    }


    //Adding the highlight and info list by taking the data from the data base

    private void init() {
        list = (ListView) findViewById(R.id.list_id);
        dataBaseHelper = new DataBaseHelper(this);

        Intent intent=getIntent();
        String name= intent.getStringExtra("Name");
        Cursor res= dataBaseHelper.getDataOfProducts(name);

        while (res.moveToNext()) {
            highlightList = new ArrayList<RowData>();
            highlightList.add(new RowData("Key Features", res.getString(8)));
            highlightList.add(new RowData("Ingredients", res.getString(9)));
            highlightList.add(new RowData("Shell Life", res.getString(10)));
            highlightList.add(new RowData("Disclaimer", res.getString(11)));
            infoList = new ArrayList<RowData>();
            infoList.add(new RowData("Manufactured", res.getString(12)));
            infoList.add(new RowData("Manufacturer", res.getString(13)));
            infoList.add(new RowData("Selling", res.getString(14)));

        }
    }
}
