package com.hina.shoppingapplication.com.capgemini.application.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.hina.shoppingapplication.com.capgemini.application.description.AboutTeam;
import com.hina.shoppingapplication.com.capgemini.application.description.AboutUs;
import com.hina.shoppingapplication.com.capgemini.application.db.DataBaseHelper;
import com.hina.shoppingapplication.com.capgemini.application.adapter.FeedbackAdapter;
import com.hina.shoppingapplication.com.capgemini.application.model.FeedbackModel;
import com.hina.shoppingapplication.com.capgemini.application.adapter.ProductAdapter;
import com.hina.shoppingapplication.com.capgemini.application.model.ProductModel;
import com.hina.shoppingapplication.R;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.ArrayList;

/*
*<p>
* Created by:Sandeep,Hina,Srikanth,Nisha,Divya,Vineetha
* 15/05/2019
* </p>
*
* In this activity we have
* Navigation Bar,
* 3 Recycler Views,
* 1 carosoul view and
* 1 feedback Recycler View
* The Main activity contains Navigation bar functionalities, which are Location,Login,MyCart,Rating,AboutUs and AboutTeam
* Here we defined all the values which are used in the app and inserted them in the DataBase
*
 */

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, AdapterView.OnItemSelectedListener {


    private int[] imageList = new int[]{R.drawable.parachute_oil, R.drawable.toor_dal, R.drawable.besan, R.drawable.kabuli_chana, R.drawable.sooji_rava, R.drawable.sugar, R.drawable.atta, R.drawable.ghee, R.drawable.sunflower, R.drawable.butter, R.drawable.maggie, R.drawable.kisan, R.drawable.chips, R.drawable.rice, R.drawable.dry_fruit, R.drawable.salt, R.drawable.penuts, R.drawable.chilli_powder, R.drawable.maida, R.drawable.almond, R.drawable.garlic_paste, R.drawable.milk, R.drawable.sambar_masala};
    private String[] offerList = new String[]{"20% OFF", "47% OFF", "30% OFF", "23% OFF", "40% OFF", "41% OFF", "14% OFF", "8% OFF", "23% OFF", "2% OFF", "6% OFF", "14% OFF", "37% OFF", "33% OFF", "5% OFF", "25% OFF", "25% OFF", "6% OFF", "35% OFF", "37% OFF", "2% OFF", "20% OFF", "50% OFF"};
    private String[] priceList = new String[]{"₹198", "₹79", "₹42", "₹92", "₹36", "₹38", "₹320", "₹395", "₹96", "₹45", "₹45", "₹107", "₹39", "₹659", "₹570", "₹26", "₹135", "₹27", "₹18", "₹178", "₹40", "₹96", "₹32"};
    private String[] clubPriceList = new String[]{"₹195", "₹76", "₹40", "₹90", "₹32", "₹34", "₹310", "₹390", "₹90", "₹42", "₹41", "₹102", "₹35", "₹650", "₹565", "₹22", "₹130", "₹23", "₹16", "₹175", "₹38", "₹95", "₹30"};
    private String[] strikePriceList = new String[]{"₹̶2̶5̶0̶", "₹̶1̶5̶0̶", "₹̶6̶0̶", "₹̶1̶2̶0̶", "₹̶6̶0̶", "₹̶6̶5̶", "₹̶3̶7̶5̶", "₹̶4̶3̶0̶", "₹̶1̶2̶5̶", "₹̶4̶6̶", "₹̶4̶8̶", "₹̶1̶2̶5̶", "₹̶6̶2̶", "₹̶9̶8̶5̶", "₹̶5̶9̶5̶", "₹̶3̶5̶", "₹̶1̶8̶0̶", "₹̶2̶9̶", "₹̶2̶8̶", "₹̶2̶8̶5̶", "₹̶4̶4̶", "₹̶1̶2̶0̶", "₹̶6̶4̶"};
    private String[] imageNameList = new String[]{"Parachute Oil", "Toor Dal", "Besan", "Kabuli Chana", "Sooji/Rava", "Sugar", "Aashirvaad Atta", "Amul Pure Ghee", "Sunlite oil", "Amul Butter", "Maggie Noodles", "Kissan Ketchup", "Banana Chips", "basmathi rice", "dried fruits", "Tata Salt", " peanuts", "Chilli Powder", "Maida", "Almonds", "Garlic Paste", "Coconut Milk", "Sambar Masala"};
    private String[] quantityList = new String[]{"600ml", "1kg", "500gm", "1kg", "1kg", "1kg", "10kg", "1 pouch", "1 ltr", "100gm", "4 packets", "950gm", "170gm", "5kg", "250gm", "1kg", "1kg", "100gm", "500gm", "200gm", "200gm", "400ml", "100gm"};
    private String[] keyFeatures = new String[]{
            "Pure and natural coconut oil. Keeps Hai Strong and Nourished. Promotes Hair Growth.",
            "Carefully Pickec for the Highest Quality. Wholesome, Healthy and Nutritious. Rich Source of essential Nutrients, fibres and protien.",
            "Made from 100% pur chana dal. Rich in essential nutrients. Used in preparation of Sweets and savouries.",
            "Rich source of protein. Healthy and tasty. Useful in preparing variety of dishes such as Falafel and Hummus.",
            "Made from hard durun wheat. Wholesome and Nutritious. Adds a distinctive texture to receipes.",
            "Hygenically Processed. Sparkling White and Pure Crystals.",
            "Made with superior quality wheat . Prepares soft and delicious roti Rich source of Fibre.",
            "Per 100 ml Energy: 814 kcal Energy from Fat: 814 kcal Total Fat: 90.5 g Saturated fat: 58 g Cholesterol: 190 mg Sodium: 0 mg Total Carbohydrate: 0 g Sugar: 0 g Protein: 0 g Vitamin A: 78 mcg Amul Ghee is used for Cooking, Garnishing and Making Sweets.",
            "Fortified with the goodness of Vitamins A and D Light, odourless and healthy oil Helps in keeping your heart and bones healthy Provides good vision to the eyes Ideal for deep frying",
            "Made from the finest cream Natural and pure",
            "MAGGI 2-Minute Masala Noodles is an instant noodles brand manufactured by Nestle Made with the finest quality spices and ingredients Each portion (70g) provides 15% of your daily Iron requirement Ready in 2 minutes, perfect meal for a hang out or house party Contains a noodle cake and the Favourite Masala Tastemaker inside, for a quick 2 minute preparation",
            "Kissan sources 100% of its tomatoes from sustainable sources thereby helping create and support several local smallholder farmers livelihoods Is easy to pour and use and can be enjoyed with every snack Made with 100% real tomatoes ",
            "Crunchy triangle shaped crisps With the rich and tangy twist of tomato Delicious and appetizing snack Perfect for those untimely hunger pangs",
            "Carefully picked for the highest quality Long sized rice having a rich flavour Has naturally appetizing taste and aroma Turns even every day's simple recipe into a delight",
            "Cashews promote bone and teeth health and also aid digestion. It helps to boost the body's metabolism",
            "Vital Indian kitchen ingredient to liven up any dish Packed with requisite amount of iodine Ensures proper mental and development of children Just add it to make your dishes delectable and wholesome",
            "The antioxidants and folic acid in peanuts may provide some protection against cancer. It is rich in calcium which helps promotes healthy bones. It has higher bioflavonoid resveratrol content than grapes. This bioflavonoid is believed to improve blood flow in the brain by as much as 30%, thus greatly reducing the risk of stroke.",
            "Finely grounded powder Enhances taste and colour of food",
            "Finely milled and refined All purpose flour Smooth and fine in texture",
            "Delectably crispy and crunchy Gurbandi Almonds Gurbandi Almonds has high content of Almond oil Wholesome, hearty and tasty",
            "Thick and grainy paste. Strong distinctive aroma.",
            "Dabur Hommade goes beyond its popular range of cooking pastes to elevate the tastes of Indian homemade recipes with Coconut Milk. Made from the first pressed extract of top-grade coconuts, this culinary delight carries a rich taste and thick texture that makes your dishes much fuller and creamier.",
            "Relish the authentic South Indian taste. Used as a flavouring agent. Imparts a rich texture and tempting aroma."};


    private String[] packagingType = new String[]{"Bottle", "Packet", "Packet", "Packet", "Packet", "Packet", "Packet", "Packet", "Packet", "Packet", "Packet", "Packet", "Packet", "Packet", "Box", "Packet", "Packet", "Packet", "Packet", "Packet", "Packet", "Packet", "Packet"};

    private String[] shelfLife = new String[]{" 6 months", "3 months", "3 months", "6 months", "3 months", "12  months", "3 months", " 3 months", " 6 months", " 3 months", " 6 months", " 6 months", " 3 months", " 12 months", " 3 months", "6 months", "  3 months", "9 months", " 3 months", "6 months", "12 months", "1 month", "3 months"};

    private String[] disclaimer = new String[]{
            "Every effort  is made to maintain accuracy of all information. However, actual product Packaging and materials may contain more and /or different information. It is recommended not to solely rellay on the information presented.",
            "Every effort  is made to maintain accuracy of all information. However, actual product Packaging and materials may contain more and /or different information. It is recommended not to solely rellay on the information presented.",
            "Every effort  is made to maintain accuracy of all information. However, actual product Packaging and materials may contain more and /or different information. It is recommended not to solely rellay on the information presented.",
            "Every effort  is made to maintain accuracy of all information. However, actual product Packaging and materials may contain more and /or different information. It is recommended not to solely rellay on the information presented.",
            "Every effort  is made to maintain accuracy of all information. However, actual product Packaging and materials may contain more and /or different information. It is recommended not to solely rellay on the information presented.",
            "Every effort  is made to maintain accuracy of all information. However, actual product Packaging and materials may contain more and /or different information. It is recommended not to solely rellay on the information presented.",
            "Every effort  is made to maintain accuracy of all information. However, actual product Packaging and materials may contain more and /or different information. It is recommended not to solely rellay on the information presented.",
            "Every effort  is made to maintain accuracy of all information. However, actual product Packaging and materials may contain more and /or different information. It is recommended not to solely rellay on the information presented.",
            "Every effort  is made to maintain accuracy of all information. However, actual product Packaging and materials may contain more and /or different information. It is recommended not to solely rellay on the information presented.",
            "Every effort  is made to maintain accuracy of all information. However, actual product Packaging and materials may contain more and /or different information. It is recommended not to solely rellay on the information presented.",
            "Every effort  is made to maintain accuracy of all information. However, actual product Packaging and materials may contain more and /or different information. It is recommended not to solely rellay on the information presented.",
            "Every effort  is made to maintain accuracy of all information. However, actual product Packaging and materials may contain more and /or different information. It is recommended not to solely rellay on the information presented.",
            "Every effort  is made to maintain accuracy of all information. However, actual product Packaging and materials may contain more and /or different information. It is recommended not to solely rellay on the information presented.",
            "Every effort  is made to maintain accuracy of all information. However, actual product Packaging and materials may contain more and /or different information. It is recommended not to solely rellay on the information presented.",
            "Every effort  is made to maintain accuracy of all information. However, actual product Packaging and materials may contain more and /or different information. It is recommended not to solely rellay on the information presented.",
            "Every effort  is made to maintain accuracy of all information. However, actual product Packaging and materials may contain more and /or different information. It is recommended not to solely rellay on the information presented.",
            "Every effort  is made to maintain accuracy of all information. However, actual product Packaging and materials may contain more and /or different information. It is recommended not to solely rellay on the information presented.",
            "Every effort  is made to maintain accuracy of all information. However, actual product Packaging and materials may contain more and /or different information. It is recommended not to solely rellay on the information presented.",
            "Every effort  is made to maintain accuracy of all information. However, actual product Packaging and materials may contain more and /or different information. It is recommended not to solely rellay on the information presented.",
            "Every effort  is made to maintain accuracy of all information. However, actual product Packaging and materials may contain more and /or different information. It is recommended not to solely rellay on the information presented.",
            "Every effort  is made to maintain accuracy of all information. However, actual product Packaging and materials may contain more and /or different information. It is recommended not to solely rellay on the information presented.",
            "Every effort  is made to maintain accuracy of all information. However, actual product Packaging and materials may contain more and /or different information. It is recommended not to solely rellay on the information presented.",
            "Every effort  is made to maintain accuracy of all information. However, actual product Packaging and materials may contain more and /or different information. It is recommended not to solely rellay on the information presented."};

    private String[] manufacturedDeatails = new String[]{
            "Mari Co Limited, TTC Building, Airoli, Thane.",
            "Hands On trades pvt Ltd",
            "Mari Co Limited, TTC Building, Airoli, Thane.",
            "Hands On trades pvt Ltd",
            "Hands On trades pvt Ltd",
            "Hands On trades pvt Ltd, Hyderabad",
            "ITC Limited, 37, J.L. Nehru Road, Kolkata- 700071",
            "Kaira District Co-operative Milk Producers' Union Limited, Anand 388 001. At Food Complex Mogar, Mogar. Lic. No. - 10014021001010. Country of origin: India ",
            "Adani Wilmar Ltd., Village Dhrub, Taluka Mundra - 370421, Dist.- Kutch, Gujarat, India ",
            "Kaira District Co-operative Milk Producers' Union Limited, Anand 388 001. At Food Complex Mogar, Mogar. Lic. No. - 10014021001010.",
            "NESTLE INDIA LIMITED, 100/101, World Trade Centre, Barakhamba lane, NEW DELHI- 110001 ",
            "Hindustan Unilever Limited, Unilever House, B D Sawant Marg, Chakala, Anderi E, Mumbai - 400099 ",
            "Chheda Specialities Foods Pvt Ltd, Gut No 115, Village Ambhai, Manor-Wada Road, Dist- Thane, 421303 ",
            "N.B. Agrotech, Assandh Road, Chirao, Karnal - 132001, Haryana India",
            "Candor foods Pvt.Ltd, Plot no.W202(A)TTD Industrial area, MiDc, off Thane Belapur Road, Kharine, Navi Mumbai 400710, Maharashtra ",
            "Tata Chemicals Limited, Leela Business Park, Andheri-kurla Road, Andheri East, Mumbai, Maharashtra - 400059 ",
            "Hands On trades pvt Ltd",
            "Hands On pvt Ltd, Hyderabad",
            "Hands On trades pvt Ltd",
            "Hands On pvt Ltd, Hyderabad",
            "Dabur India Limited Registered Office: 8/3, Asaf Ali Road, New Delhi-110002",
            "Hands On pvt Ltd, Hyderabad",
            "Hands On pvt Ltd, Hyderabad"
    };

    private String[] marketedBy = new String[]{
            "Mari Co Limited, TTC Building, Airoli, Thane.",
            "Mari Co Limited, TTC Building, Airoli, Thane.",
            "Mari Co Limited, TTC Building, Airoli, Thane.",
            "Mari Co Limited, TTC Building, Airoli, Thane.",
            "Mari Co Limited, TTC Building, Airoli, Thane.",
            "Mari Co Limited, TTC Building, Airoli, Thane.",
            "Mari Co Limited, TTC Building, Airoli, Thane.",
            "Kaira District Co-operative Milk Producers' Union Limited, Anand 388 001. At Food Complex Mogar, Mogar. Lic. No. - 10014021001010. Country of origin: India ",
            "Adani Wilmar Limited, Fortune House, Near Navrangpura Railway Crossing, Ahmedabad - 380009, Gujarat, India",
            "Kaira District Co-operative Milk Producers' Union Limited, Anand 388 001. At Food Complex Mogar, Mogar. Lic. No. - 10014021001010.",
            "NESTLE INDIA LIMITED, 100/101, World Trade Centre, Barakhamba lane, NEW DELHI- 110001 ",
            "Hindustan Unilever Limited, Unilever House, B D Sawant Marg, Chakala, Anderi E, Mumbai - 400099 ",
            "Chheda Specialities Foods Pvt Ltd, Gut No 115, Village Ambhai, Manor-Wada Road, Dist- Thane, 421303",
            "Adani Wilmar Ltd, Fortune House, Near Navrangpura Railway Crossing, Ahmedabad - 380009 Gujarat India ",
            "Candor foods Pvt.Ltd, Plot no.W202(A)TTD Industrial area, MiDc, off Thane Belapur Road, Kharine, Navi Mumbai 400710, Maharashtra ",
            "Tata Chemicals Limited, Leela Business Park, Andheri-kurla Road, Andheri East, Mumbai, Maharashtra - 400059", "Mari Co Limited, TTC Building, Airoli, Thane.",
            "Hindustan Unilever Limited, Unilever House, B D Sawant Marg, Chakala, Anderi E, Mumbai - 400099 ",
            "NESTLE INDIA LIMITED, 100/101, World Trade Centre, Barakhamba lane, NEW DELHI- 110001 ",
            "Mari Co Limited, TTC Building, Airoli, Thane.",
            "NESTLE INDIA LIMITED, 100/101, World Trade Centre, Barakhamba lane, NEW DELHI- 110001 ",
            "Hindustan Unilever Limited, Unilever House, B D Sawant Marg, Chakala, Anderi E, Mumbai - 400099 ",
            "Mari Co Limited, TTC Building, Airoli, Thane.",
            "Mari Co Limited, TTC Building, Airoli, Thane."};

    private String[] seller = new String[]{
            "LA Super Retail pvt Ltd",
            "LA Super Retail pvt Ltd",
            "LA Super Retail pvt Ltd",
            "LA Super Retail pvt Ltd",
            "LA Super Retail pvt Ltd",
            "LA Super Retail pvt Ltd",
            "Crofcloud Onsale Landing Pvt Ltd",
            "LA Super Retail pvt Ltd",
            "Crofcloud Onsale Landing Pvt Ltd",
            "Crofcloud Onsale Landing Pvt Ltd",
            "Crofcloud Onsale Landing Pvt Ltd",
            "LA Super Retail pvt Ltd",
            "LA Super Retail pvt Ltd",
            "Crofcloud Onsale Landing Pvt Ltd",
            "Crofcloud Onsale Landing Pvt Ltd",
            "Crofcloud Onsale Landing Pvt Ltd",
            "Crofcloud Onsale Landing Pvt Ltd",
            "Crofcloud Onsale Landing Pvt Ltd",
            "Crofcloud Onsale Landing Pvt Ltd",
            "Crofcloud Onsale Landing Pvt Ltd",
            "Crofcloud Onsale Landing Pvt Ltd",
            "Crofcloud Onsale Landing Pvt Ltd",
            "Crofcloud Onsale Landing Pvt Ltd"};

    int[] descriptionImage1 = new int[]{R.drawable.parachut_oil_d1, R.drawable.toor_dal_d1, R.drawable.besan_d1, R.drawable.kabuli_chana_d1, R.drawable.sooji_d1,
            R.drawable.sugar_d1, R.drawable.atta_d1, R.drawable.ghee_d1, R.drawable.sunflower_oil_d1, R.drawable.butter_d1,
            R.drawable.maggi_d1, R.drawable.ketchup_d1, R.drawable.banana_chips_d1, R.drawable.fotune_biryani_rice_d1, R.drawable.dried_fruits_d1,
            R.drawable.salt_d1, R.drawable.peanut_d1, R.drawable.chilli_powder_d1, R.drawable.maida_d1, R.drawable.almonds_d1, R.drawable.garlic_paste_d1,
            R.drawable.coconut_milk_d1, R.drawable.sambar_masala_d1};
    int[] descriptionImage2 = new int[]{R.drawable.parachut_oil_d2, R.drawable.toor_dal_d2, R.drawable.besan_d2, R.drawable.kabuli_chana_d2, R.drawable.sooji_d2,
            R.drawable.sugar_d2, R.drawable.atta_d2, R.drawable.ghee_d2, R.drawable.sunflower_oil_d2, R.drawable.butter_d2,
            R.drawable.maggi_d2, R.drawable.ketchup_d2, R.drawable.banana_chips_d2, R.drawable.fotune_biryani_rice_d2, R.drawable.dried_fruits_d2,
            R.drawable.salt_d2, R.drawable.peanut_d2, R.drawable.chilli_powder_d2, R.drawable.maida_d2, R.drawable.almonds_d2, R.drawable.garlic_paste_d2,
            R.drawable.coconut_milk_d2, R.drawable.sambar_masala_d2};

    int[] carouselImages = new int[]{R.drawable.carousel_image_1, R.drawable.carousel_image_2,
            R.drawable.carousel_image_3, R.drawable.carousel_image_4, R.drawable.carousel_image_5};
    private int[] fbImageList = new int[]{R.drawable.person_icon,R.drawable.person_icon, R.drawable.person_icon,R.drawable.person_icon,R.drawable.person_icon};
    private String[] messageList = new String[]{
            "\"I've been a Grofers customer for more than a year now and have done purchases of over 1lac rupees. Grofers...\"",
            "\"Mai 3.5 saal se Grofers se shopping kar rahi hoon. Best experience raha hai mera, service bhi good, team bhi response...\"",
            "\"There are lot of offers provided by grofers like bank offers that help you save a lot of money. I saw a trend wher...\"",
            "\"Awesome savings. They give good freebies, we get a very good chance to earn points and get free products!!\"",
            "\"When I book an item with Grofers and if I find that the same item is available at a better price at another place.\""};


    private String[] nameList = new String[]{
            "Manish Mathur, Delhi",
            "Sangeeta Vora, Ahemdabad",
            "Akash Verma, Bengaluru",
            "Amrutha Krishna, Bengaluru",
            "Neeraj Vallecha, Delhi"};


    //Defined all the views

    private ImageView cart;
    String str = "Airoli";

    private RecyclerView recyclerView;
    private RecyclerView recyclerView1;
    private RecyclerView recyclerView2;
    private ArrayList<ProductModel> imageModelArrayList;
    private ArrayList<ProductModel> imageModelArrayList1;
    private ArrayList<ProductModel> imageModelArrayList2;
    private ProductAdapter adapter;
    private ProductAdapter adapter1;
    private ProductAdapter adapter2;
    DataBaseHelper dataBaseHelper;
    CarouselView carouselView;
    private ImageView axis;
    private ImageView hdfc;
    private ImageView icici;
    private ImageView idbi;
    private ImageView kotak;
    private ImageView rbl;


    private RecyclerView fb_recyclerView;
    private FeedbackAdapter fb_adapter;
    private Dialog rankDialog;
    private RatingBar ratingBar;
    private String bankName;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cart = findViewById(R.id.cartId);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        //RECEIVES LOCATION NAME
        Intent intentlocation = getIntent();
        String str = intentlocation.getStringExtra("location");

        //CODE TO DYNAMICALLY INPUT MENU LOCATION NAME
        NavigationView navigationView1 = (NavigationView) findViewById(R.id.nav_view);

        // get menu from navigationView
        Menu menu = navigationView1.getMenu();

        // find MenuItem you want to change
        MenuItem nav_location = menu.findItem(R.id.nav_location);

        // set new title to the MenuItem
        nav_location.setTitle(str);

        // add NavigationItemSelectedListener to check the navigation clicks
        navigationView1.setNavigationItemSelectedListener(this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CartActivity.class);
                startActivity(intent);
            }
        });


        // recycler view implementation

        recyclerView = (RecyclerView) findViewById(R.id.recycler1);
        recyclerView1 = (RecyclerView) findViewById(R.id.recycler2);
        recyclerView2 = (RecyclerView) findViewById(R.id.recycler3);
        fb_recyclerView = (RecyclerView) findViewById(R.id.fb_recycler);

        imageModelArrayList = bestSellingItems();
        adapter = new ProductAdapter(this, imageModelArrayList);
        imageModelArrayList1 = topSaversToday();
        adapter1 = new ProductAdapter(this, imageModelArrayList1);
        imageModelArrayList2 = blockbusterOffers();
        adapter2 = new ProductAdapter(this, imageModelArrayList2);
        recyclerView.setAdapter(adapter);
        recyclerView1.setAdapter(adapter1);
        recyclerView2.setAdapter(adapter2);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView1.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView2.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));


        //Inserted the data into the database
        dataBaseHelper = new DataBaseHelper(this);
        int itemsLength = imageList.length;
        for (int i = 0; i < itemsLength; i++) {
            boolean isInserted = dataBaseHelper.insertItemData(
                    imageNameList[i],
                    imageList[i],
                    descriptionImage1[i],
                    descriptionImage2[i],
                    strikePriceList[i],
                    priceList[i],
                    quantityList[i],
                    offerList[i],
                    keyFeatures[i],
                    packagingType[i],
                    shelfLife[i],
                    disclaimer[i],
                    manufacturedDeatails[i],
                    marketedBy[i],
                    seller[i],
                    clubPriceList[i]);

            if (isInserted) {
                Log.d("TAG", "Inserted the Data of " + imageNameList[i]);
            } else {
                Log.d("TAG", "Not Inserted the Data of " + imageNameList[i]);
            }

        }

        //Search Functionality

        Spinner spinner = (Spinner) findViewById(R.id.search);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.product_names, android.R.layout.simple_spinner_item);
        // Specify layout to be used when list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Applying the adapter to our spinner
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(MainActivity.this);

        //Carousel View implementation
        carouselView = (CarouselView) findViewById(R.id.carouselView);
        carouselView.setPageCount(carouselImages.length);

        carouselView.setImageListener(imageListener);

        axis = (ImageView) findViewById(R.id.axis);
        hdfc = (ImageView) findViewById(R.id.hdfc);
        icici = (ImageView) findViewById(R.id.icici);
        idbi = (ImageView) findViewById(R.id.idbi);
        kotak = (ImageView) findViewById(R.id.kotak);
        rbl = (ImageView) findViewById(R.id.rbl);


        axis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bankIntent=new Intent(MainActivity.this,activity_bank_alert_dialog.class);
                bankName="Axis";
                bankIntent.putExtra("Bank",bankName);
                startActivity(bankIntent);

            }
        });


        hdfc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bankIntent=new Intent(MainActivity.this,activity_bank_alert_dialog.class);
                bankName="HDFC";
                bankIntent.putExtra("Bank",bankName);
                startActivity(bankIntent);

            }
        });


        icici.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bankIntent=new Intent(MainActivity.this,activity_bank_alert_dialog.class);
                bankName="ICICI";
                bankIntent.putExtra("Bank",bankName);
                startActivity(bankIntent);

            }
        });


        kotak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bankIntent=new Intent(MainActivity.this,activity_bank_alert_dialog.class);
                bankName="Kotak";
                bankIntent.putExtra("Bank",bankName);
                startActivity(bankIntent);

            }
        });


        idbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bankIntent=new Intent(MainActivity.this,activity_bank_alert_dialog.class);
                bankName="IDBI";
                bankIntent.putExtra("Bank",bankName);
                startActivity(bankIntent);

            }
        });


        rbl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bankIntent=new Intent(MainActivity.this,activity_bank_alert_dialog.class);
                bankName="RBL";
                bankIntent.putExtra("Bank",bankName);
                startActivity(bankIntent);

            }
        });



        //FeedBack Recycler View implementation
        ArrayList<FeedbackModel> fbImgModelArrayList = customerFeedback();
        fb_adapter = new FeedbackAdapter(this, fbImgModelArrayList);
        fb_recyclerView.setAdapter(fb_adapter);
        fb_recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));



    }

    //ImageListener of the Carousel View

    ImageListener imageListener = new ImageListener() {

        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(carouselImages[position]);
        }
    };


    //When Item is selected in the recycler view

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Log.d("TAG", "In Item Selected");
        String selectedItem = parent.getItemAtPosition(position).toString();
        Intent intent = new Intent(this, ProductDescription.class);
        intent.putExtra("Name", selectedItem);
        startActivity(intent);

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    // Array List of the BestSellingItems
    private ArrayList<ProductModel> bestSellingItems() {

        ArrayList<ProductModel> bestSellingList = new ArrayList<>();

        for (int i = 0; i < 23; i++) {
            ProductModel productModel = new ProductModel();
            productModel.setName(imageNameList[i]);
            productModel.setImage_drawable(imageList[i]);
            productModel.setPrice(priceList[i]);
            productModel.setOffer(offerList[i]);
            productModel.setStrike_price(strikePriceList[i]);
            productModel.setQuantity(quantityList[i]);
            productModel.setClubPrice(clubPriceList[i]);
            bestSellingList.add(productModel);
        }

        return bestSellingList;
    }

    // Array List of the Top Savers Today
    private ArrayList<ProductModel> topSaversToday() {

        ArrayList<ProductModel> topSaversList = new ArrayList<>();

        for (int i = 0; i < 23; i++) {

            if (i == 18 || i == 14 || i == 4 || i == 6 || i == 8 || i == 10 || i == 12 || i == 2 || i == 16 || i == 21 || i == 20 || i == 22) {
                ProductModel productModel = new ProductModel();
                productModel.setName(imageNameList[i]);
                productModel.setImage_drawable(imageList[i]);
                productModel.setPrice(priceList[i]);
                productModel.setOffer(offerList[i]);
                productModel.setStrike_price(strikePriceList[i]);
                productModel.setQuantity(quantityList[i]);
                productModel.setClubPrice(clubPriceList[i]);
                topSaversList.add(productModel);
            }
        }
        return topSaversList;
    }

    // Array List of the Block Buster Offers
    private ArrayList<ProductModel> blockbusterOffers() {

        ArrayList<ProductModel> blockbusterList = new ArrayList<>();

        for (int i = 0; i < 23; i++) {

            if (i == 18 || i == 4 || i == 12 || i == 1 || i == 22 || i == 2 || i == 19 || i == 5 || i == 13) {
                ProductModel productModel = new ProductModel();
                productModel.setName(imageNameList[i]);
                productModel.setImage_drawable(imageList[i]);
                productModel.setPrice(priceList[i]);
                productModel.setOffer(offerList[i]);
                productModel.setStrike_price(strikePriceList[i]);
                productModel.setQuantity(quantityList[i]);
                productModel.setClubPrice(clubPriceList[i]);
                blockbusterList.add(productModel);
            }
        }


        return blockbusterList;
    }


    //Click functionality on pressing back
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    //Handles onClicks of Navigatiobn bar Menu
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        // Handle navigation view item clicks here.
        int id = menuItem.getItemId();


        if (id == R.id.nav_location) {
            // Handle the location action
            Toast toast = Toast.makeText(getApplicationContext(), "Your Clicked location Page", Toast.LENGTH_LONG);
            toast.show();
            Intent intent = new Intent(MainActivity.this, LocationActivity.class);
            startActivity(intent);


        } else if (id == R.id.nav_login) {

            Intent intentl = new Intent(this, Login.class);
            startActivity(intentl);




        } else if (id == R.id.nav_rateus) {

            Intent intent = new Intent(MainActivity.this, com.hina.shoppingapplication.com.capgemini.application.activity.RatingBar.class);
            startActivity(intent);



           // Intent intent1 = new Intent(this, Rating.class);
          //  startActivity(intent1);

        } else if (id == R.id.nav_aboutus) {
            Intent menuIntent = new Intent(this, AboutUs.class);
            startActivity(menuIntent);


        } else if (id == R.id.nav_aboutteam) {
            Intent menuIntent = new Intent(this, AboutTeam.class);
            startActivity(menuIntent);

        }

        return true;
    }


   //The Feed back recycler views array
    private ArrayList<FeedbackModel> customerFeedback(){

        ArrayList<FeedbackModel> feedbackList = new ArrayList<>();

        for(int i = 0; i < 5; i++){
            FeedbackModel feedbackModel = new FeedbackModel();
            feedbackModel.setName(nameList[i]);
            feedbackModel.setFbCustomerImage(fbImageList[i]);
            feedbackModel.setMessage(messageList[i]);
            feedbackList.add(feedbackModel);
        }

        return feedbackList;
    }



}



