package com.hina.shoppingapplication.com.capgemini.application.description;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import com.hina.shoppingapplication.R;

/*
 * Created By: Sandeep
 *
 * Calls The HTML file AboutUs.html from assets folder which consists of information about the app
 *
 * */


public class AboutUs extends AppCompatActivity {

    public String fileName = "AboutUs.html";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_us);

        setTitle("About Us");
        WebView webView;
        // init webView
        webView = (WebView) findViewById(R.id.simpleWebView);
        // displaying content in WebView from html file that stored in assets folder
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("file:///android_asset/" + fileName);


    }
}
