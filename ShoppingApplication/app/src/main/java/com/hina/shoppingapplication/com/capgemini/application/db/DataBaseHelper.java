package com.hina.shoppingapplication.com.capgemini.application.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;



/*
*
* Created By : Sandeep and Vineetha
*
* Class Database Helper used to define the structure of Database and consisting tables
* also called in other classes to retreive details
*
*
**/

public class DataBaseHelper extends SQLiteOpenHelper {


   //Creating database Products
    public static final String DATABASE_NAME = "Products.db";

    //Creating Table PRODUCTS_TABLE and defining the columns
    public static final String PRODUCTS_TABLE = "PRODUCTS_TABLE";
    public static final String COL_NAME = "NAME";
    public static final String COL_IMAGE = "IMAGE";
    public static final String COL_DESCRIPTION_IMAGE_1 = "DESCRIPTION_IMAGE_1";
    public static final String COL_DESCRIPTION_IMAGE_2 = "DESCRIPTION_IMAGE_2";
    public static final String COL_ORIGINAL_PRICE = "ORIGINAL_PRICE";
    public static final String COL_DISCOUNT_PRICE = "DISCOUNT_PRICE";
    public static final String COL_QUANTITY = "QUANTITY";
    public static final String COL_DISCOUNT_PERC = "DISCOUNT_PERCENTAGE";
    public static final String COL_KEYFEATURES = "KEY_FEATURES";
    public static final String COL_PACKAGING_TYPE = "PACKAGING_TYPE";
    public static final String COL_SHELF_LIFE = "SHELF_LIFE";
    public static final String COL_DISCLAIMER = "DISCLAIMER";
    public static final String COL_MANUFACTURER_DETAILS = "MANUFACTURER_DETAILS";
    public static final String COL_MARKETED_BY = "MARKETED_BY";
    public static final String COL_SELLER = "SELLER";
    public static final String COL_CLUB_PRICE = "CLUB_PRICE";



    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    //OnCreate method to create table
    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table " + PRODUCTS_TABLE + "" +
                " (NAME TEXT," +
                "IMAGE TEXT," +
                "DESCRIPTION_IMAGE_1 TEXT," +
                "DESCRIPTION_IMAGE_2 TEXT," +
                "ORIGINAL_PRICE TEXT," +
                "DISCOUNT_PRICE TEXT," +
                "QUANTITY TEXT," +
                "DISCOUNT_PERCENTAGE TEXT," +
                "KEY_FEATURES TEXT," +
                "PACKAGING_TYPE TEXT," +
                "SHELF_LIFE TEXT," +
                "DISCLAIMER TEXT," +
                "MANUFACTURER_DETAILS TEXT," +
                "MARKETED_BY TEXT," +
                "SELLER TEXT,"+
                "CLUB_PRICE TEXT)");
        Log.d("TAG", "PRODUCTS Table is Created!");


    }


    //onUpgrade Function
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


        db.execSQL("DROP TABLE IF EXISTS " + PRODUCTS_TABLE);
        onCreate(db);

    }

    //inserting the data in the product table
    public boolean insertItemData(String name,
                                  int image,
                                  int description_image_1,
                                  int description_image_2,
                                  String original_price,
                                  String discount_price,
                                  String quantity,
                                  String discount_percentage,
                                  String key_features,
                                  String packaging_type,
                                  String shelf_life,
                                  String disclaimer,
                                  String manufacturer_details,
                                  String marketed_by,
                                  String seller,
                                  String clubPrice) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_IMAGE, image);
        contentValues.put(COL_NAME, name);
        contentValues.put(COL_DESCRIPTION_IMAGE_1, description_image_1);
        contentValues.put(COL_DESCRIPTION_IMAGE_2, description_image_2);
        contentValues.put(COL_ORIGINAL_PRICE, original_price);
        contentValues.put(COL_DISCOUNT_PRICE, discount_price);
        contentValues.put(COL_QUANTITY, quantity);
        contentValues.put(COL_DISCOUNT_PERC, discount_percentage);
        contentValues.put(COL_KEYFEATURES, key_features);
        contentValues.put(COL_PACKAGING_TYPE, packaging_type);
        contentValues.put(COL_SHELF_LIFE, shelf_life);
        contentValues.put(COL_DISCLAIMER, disclaimer);
        contentValues.put(COL_MANUFACTURER_DETAILS, manufacturer_details);
        contentValues.put(COL_MARKETED_BY, marketed_by);
        contentValues.put(COL_SELLER, seller);
        contentValues.put(COL_CLUB_PRICE, clubPrice);


        long result = db.insert(PRODUCTS_TABLE, null, contentValues);

        if (result == -1)
            return false;
        else
            return true;
    }


    //getting the data of a product from the product table
    public Cursor getDataOfProducts(String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + PRODUCTS_TABLE + " Where " + COL_NAME + " = " + "'" + name + "'", null);
        return res;
    }

}
