package com.hina.shoppingapplication.com.capgemini.application.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.hina.shoppingapplication.R;

/*
 * Created By:Sandeep
 *
 * The Rating of the application will be shown and the implementation of it is done in this class
 *
 * */

public class RatingBar extends Activity {

    private float rateValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        AlertDialog.Builder mBuild = new AlertDialog.Builder(RatingBar.this);
        View mView = getLayoutInflater().inflate(R.layout.rating_bar, null);

        final android.widget.RatingBar ratebar = (android.widget.RatingBar) mView.findViewById(R.id.ratingBar);
        ratebar.setOnRatingBarChangeListener(new android.widget.RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(android.widget.RatingBar ratingBar, float rating, boolean fromUser) {
                rateValue = rating;
                Toast.makeText(RatingBar.this, "Your Response is " + rating, Toast.LENGTH_SHORT).show();

            }
        });

        Button btnSubmit = (Button) mView.findViewById(R.id.btnSubRating);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(RatingBar.this, "" + rateValue, Toast.LENGTH_SHORT).show();

                Intent intent1 = new Intent(RatingBar.this, MainActivity.class);
                startActivity(intent1);
                Toast.makeText(RatingBar.this, "Thanks for Rating the app", Toast.LENGTH_SHORT).show();


            }
        });

        mBuild.setView(mView);
        AlertDialog dialog = mBuild.create();
        dialog.show();

    }

}

