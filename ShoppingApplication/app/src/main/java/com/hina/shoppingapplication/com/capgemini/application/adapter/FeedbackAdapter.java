package com.hina.shoppingapplication.com.capgemini.application.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hina.shoppingapplication.com.capgemini.application.model.FeedbackModel;
import com.hina.shoppingapplication.R;

import java.util.ArrayList;

/*
* Created By:Nisha
*
* This adapter will set data of the name, image and message of the list in the recycler view of the feedback
*
*
* */

public class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.MyViewHolder> {

    private LayoutInflater inflater;

    private ArrayList<FeedbackModel> fbImgModelArrayList;


    public FeedbackAdapter(Context ctx, ArrayList<FeedbackModel> fbImgModelArrayList){

        inflater = LayoutInflater.from(ctx);
        this.fbImgModelArrayList = fbImgModelArrayList;
    }

    @Override
    public FeedbackAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.recycler_feedback, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(FeedbackAdapter.MyViewHolder holder, int position) {

        holder.customerImage.setImageResource(fbImgModelArrayList.get(position).getFbCustomerImage());
        holder.customerName.setText(fbImgModelArrayList.get(position).getName());
        holder.customerMessage.setText(fbImgModelArrayList.get(position).getMessage());
    }

    @Override
    public int getItemCount() {

        return fbImgModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView customerName;
        ImageView customerImage;
        TextView customerMessage;

        public MyViewHolder(View itemView) {
            super(itemView);

            customerName = (TextView) itemView.findViewById(R.id.txt_name);
            customerImage = (ImageView) itemView.findViewById(R.id.iv);
            customerMessage = (TextView) itemView.findViewById(R.id.txt_message);
        }

    }
}
