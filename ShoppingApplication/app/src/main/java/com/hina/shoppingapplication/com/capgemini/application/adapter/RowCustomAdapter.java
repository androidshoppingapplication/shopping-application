package com.hina.shoppingapplication.com.capgemini.application.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.hina.shoppingapplication.R;
import com.hina.shoppingapplication.com.capgemini.application.model.RowData;

import java.util.ArrayList;


/*
 * Created By:Vineetha
 *
 * This adapter will set all the data of the list for showing in the list for highlights and the information of the product
 *
 *
 * */

public class RowCustomAdapter  extends ArrayAdapter<RowData> {

    private Context context;
    private ArrayList<RowData> arrayList;
    public RowCustomAdapter(Context context, int resource, ArrayList<RowData> arrayList) {
        super(context, resource, arrayList);
        this.context = context;
        this.arrayList = arrayList;
    }
    public View getView(int position, View convertView, ViewGroup parent){
        View listItem = convertView;
        if (convertView == null) {
            listItem = LayoutInflater.from(context).inflate(R.layout.activity_product_description_row_list,parent,false);
        }

        RowData data=arrayList.get(position);
        TextView header = (TextView)listItem.findViewById(R.id.header_id);
        header.setText(data.getHeader());

        TextView description = (TextView)listItem.findViewById(R.id.description_id);
        description.setText(data.getDescription());

        return listItem;

    }

}
