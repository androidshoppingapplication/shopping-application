package com.hina.shoppingapplication.com.capgemini.application.model;


/*
* Created By:Hina
*
* The Product Images pojo class setting and getting the values
*
* */
public class ProductImages {

    private int image_drawable;

    public int getImage_drawable() {
        return image_drawable;
    }

    public void setImage_drawable(int image_drawable) {
        this.image_drawable = image_drawable;
    }

}
