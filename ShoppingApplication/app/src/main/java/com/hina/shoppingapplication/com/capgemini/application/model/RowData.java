package com.hina.shoppingapplication.com.capgemini.application.model;


/*
 * Created By:Vineetha
 *
 * The Row Data pojo class setting and getting the values
 *
 * */
public class RowData {

    String header;
    String description;

    public void setHeader(String header) {
        this.header = header;
    }

    public String getHeader() {
        return header;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public RowData(String header,String description){this.header = header;this.description = description;}

}
